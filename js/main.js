(function($){

  console.log("main.js");

  WebFont.load({
		google: {
			families: ['Changa+One', 'Poppins:ital,wght@0,400;0,700;1,400']
		}
	});

  $('#image-carousel').slick({
    infinite: true,
    slidesToShow: 4,
    dots: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  $('#books-carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('#experts-carousel').slick({
    infinite: true,
    slidesToShow: 1,
    dots: true,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="bi bi-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="bi bi-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false
        }
      }
    ]
  });

  $('#reviews-carousel').slick({
    infinite: true,
    slidesToShow: 1,
    dots: true,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="bi bi-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="bi bi-chevron-right"></i></button>'
  });

  $('.masonry').masonry();

  $('.slideto').click(function(e) {
    e.preventDefault();
    var section = $(this).attr('href');
    $([document.documentElement, document.body]).animate({
      scrollTop: $(section).offset().top
    }, 500);
  });

  $('#membership .product button').click(function(e) {

    var price = $(this).parents('.product').data('price');

    $(this).parents('.product').toggleClass('selected');
    $('#membership .product').not($(this).parents('.product')).removeClass('selected');
    $('#dropdown-popup').collapse('show');

    if ($(this).parents('.product').hasClass('selected') == false) {
      $('#dropdown-popup').collapse('hide');
    } else {
      if ($(window).width() <= 767) {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#dropdown-popup').offset().top
        }, 500);
      }
    }

    $('#dropdown-popup').find('form button span').html(price);

  });

  window.addEventListener('load', function () {
    $('.masonry').masonry();
  });

  if ($('#countdown').length > 0) {

    // Set the date we're counting down to
    var countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      document.getElementById("countdown").innerHTML = hours + ":" + minutes + ":" + seconds;

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
      }
    }, 1000);

  }

})(jQuery);
